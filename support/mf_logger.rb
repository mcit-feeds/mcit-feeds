require 'mail'

class MfLogger

  def self.log msg
    @@log_method.call( msg )
  end

  def self.set_log_type type
    @@log_method =
      case type
      when :puts
        method(:puts)
      when :mail
        method(:send_mail)
      else
        nil
      end
  end
  
  def self.mail_from= from
    @@mail_from = from
  end
  
  def self.mail_to= to
    @@mail_to = to
  end
  
  def self.send_mail msg
    Mail.deliver do
      from     @@mail_from
      to       @@mail_to
      subject  'Error in Manchester citizenship feeds'
      body     msg
    end
  end
end

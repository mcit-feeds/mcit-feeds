
class MccPageLocations

  def self.meeting_types
    ROOT + MEETING_TYPES
  end


  private

  ROOT = 'http://www.manchester.gov.uk'

  MEETING_TYPES = '/site/scripts/meetings_index.php'
end

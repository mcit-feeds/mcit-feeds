require 'dm-core'


module DatamapperWrapper
  
  def init_datamapper opts = {}
    db_type = opts.delete :db_type

    case db_type
    when :sqlite3
      db_name = opts.delete :db_name
      dm_setup_string = "sqlite3://#{db_name}"
      DataMapper.setup(:default, dm_setup_string )

    when :mysql
      db_name = opts.delete :db_name
      user_name = opts.delete :db_user
      password = opts.delete :db_password
      host = opts.delete :db_host
      DataMapper.setup( :default,
                        "mysql://#{user_name}:#{password}@#{host}/#{db_name}" )

    else
      raise ArgumentError.new( "Unknown database type: #{db_type}" )
    end
    
    DataMapper::Model.raise_on_save_failure
    DataMapper.finalize
  end
end

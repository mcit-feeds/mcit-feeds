require 'rack'
require 'dm-migrations'
require 'yaml'
require 'mail'


# Get app root directory, in case app was started from another directory.
app_directory = File.expand_path( File.dirname(__FILE__) )

# Load config data.
config = YAML::load_file( "#{app_directory}/.config.yml" )
config[:root_directory] = app_directory

# Modify any sqlite3 database name to explicitly be in app root directory.
config[:db_name].prepend( "#{app_directory}/" ) if config[:db_type] == :sqlite3

# Don't set the mail delivery method in the actual app.
# That would mess up testing.
Mail.defaults do
  delivery_method :sendmail
end
    
# Run the app
require "#{app_directory}/mcit_feeds.rb"
run McitFeeds.new( config )

# Consider ditching this and writing proper migrations like a grown up.
DataMapper.auto_upgrade!
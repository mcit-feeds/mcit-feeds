require 'dm-timestamps'


class MeetingType
  include DataMapper::Resource

  property :id, Serial
  property :name, String, :length => 127, :required => true, :unique_index => true
  property :link, String, :length => 255, :required => true
  property :updated_at, DateTime

  has n, :meetings

  def to_s
    "#{@name} (updated #{updated_at}): #{@link}"
  end
  
  def url_friendly_name
    #name.gsub(/[^a-zA-Z0-9]/, ' ').strip.squeeze(' ').gsub(' ','-').downcase
    link.split('/').delete_if(&:empty?).last
  end
end


class Meeting
  include DataMapper::Resource

  property :id, Serial
  property :date, Date, :required => true, :unique_index => :dated_meeting
  property :link, String, :length => 255, :required => true
  property :updated_at, DateTime
  
  property :meeting_type_id, Integer, :required => true, :unique_index => :dated_meeting
  belongs_to :meeting_type, :required => true

  def to_s
    "#{meeting_type.name} on #{date} (updated #{updated_at}): #{@link}"
  end
end


class Feed
  include DataMapper::Resource

  property :id, Serial
  property :name, String, :length => 127, :required => true, :unique_index => :u
  property :type, String, :length => 127, :required => true, :unique_index => :u
  property :cache, Text, :length => 1000000, :required => true

  def clear_cache
    cache = ''
    save
  end
end

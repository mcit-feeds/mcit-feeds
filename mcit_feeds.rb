require 'sinatra/base'
require 'dm-core'
require 'rack'

# Add the path of the main app file to the load path
$LOAD_PATH.unshift( File.expand_path( File.dirname( __FILE__ ) ) )

require 'support/unknown_options_error.rb'
require 'support/mf_logger.rb'
require 'support/datamapper_wrapper.rb'

require 'controllers.rb'


class McitFeeds < Sinatra::Base
  include DatamapperWrapper
  
  def initialize opts = {}
    # This class is the only app. Nothing is downstream of here.
    super nil
    
    self.class.init_sinatra opts
    init_datamapper opts
    init_app opts

    raise UnknownOptionsError.new(opts) unless opts.empty?
  end

  
  def self.init_sinatra opts
    
    configure do
      set :raise_errors, true
      set :show_exceptions, false
      
      # Some startup methods work without this but other startup methods fail
      # to find the public directory (css, js, etc), hence setting :root here.
      set :root, opts.delete( :root_directory )
    end
  end

  
  def init_app opts
    MfLogger.mail_from = opts.delete :admin_email_from
    MfLogger.mail_to = opts.delete :admin_email_to
    MfLogger.set_log_type( opts.delete :log_type )
  end
end

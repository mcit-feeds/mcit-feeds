require 'sinatra/base'
require "rss"

require 'support/mf_logger.rb'
require 'support/ordinalize.rb'
require 'targets/mcc_page_locations.rb'
require 'models.rb'


class McitFeeds < Sinatra::Base

  def get_later_time latest, current
    if latest.nil? or ( current > latest )
      current
    else
      latest
    end
  end

  def get_feed_updated_time last_item_updated_time
    if last_item_updated_time
      last_item_updated_time.to_s
    else
      # To DO : What should the feed's updated time be on a feed that
      # has no items?
      Date.today.prev_year.to_s
    end
  end
    
  get '/feeds/mcc-meeting-types' do
    
    # TODO - cache

    content_type 'text/xml'

    RSS::Maker.make("atom") do |maker|
      maker.channel.author = "Manchester Citizenship"
      maker.channel.about = MccPageLocations.meeting_types
      maker.channel.title = "Manchester City Council meeting types"
      maker.channel.description = "Data scraped from the Manchester City Council website in order to provide a list of types of meeting and committee, and to allow notification of any new meeting type being added."

      last_updated = nil

      MeetingType.all().each do | mt |
        i = maker.items.new_item
        i.link = mt.link
        i.title = mt.name
        i.updated = mt.updated_at.to_s
        last_updated = get_later_time last_updated, mt.updated_at
      end

      maker.channel.updated = get_feed_updated_time last_updated
    end.to_s
  end


  def add_meetings_to_feed maker, meeting_type, latest_update

    meeting_type.meetings.each do | m |
      i = maker.items.new_item
      i.link = m.link
      i.title = "#{meeting_type.name}, " +
        m.date.strftime( "%A #{m.date.day.ordinalize} %B %Y" )
      i.updated = m.updated_at.to_s
      latest_update = get_later_time latest_update, m.updated_at
    end
    latest_update
  end


  get '/feeds/mcc-meeting/all' do
    
    # TODO - cache

    content_type 'text/xml'

    feed = Feed.first_or_create( type: Meeting.name, name: 'all' )
    return feed.cache unless feed.cache.nil? or feed.cache.empty?

    feed.cache = RSS::Maker.make("atom") do |maker|
      maker.channel.author = "Manchester Citizenship"
      maker.channel.about = MccPageLocations.meeting_types
      maker.channel.title = "All meetings (Manchester City Council)"
      maker.channel.description = "Data scraped from the Manchester City Council website in order to provide a list of all meetings, and to allow notification of any new meetings being added."
      
      last_updated = nil

      MeetingType.all.each do | mt |
        last_updated = add_meetings_to_feed maker, mt, last_updated
      end

      maker.channel.updated = get_feed_updated_time last_updated
    end.to_s

    feed.save

    feed.cache
  end
  

  get '/feeds/mcc-meeting/:name' do | name |
    
    # TODO - cache


    meeting_types = MeetingType.all( :link.like => "%#{name}" )

    if meeting_types.empty?
      halt 404

    elsif meeting_types.count > 1
      err = "Multiple meeting types found when meetings of #{name} requested: " +
        meeting_types.inject([]){|acc,mt| acc<< mt.name }.join(', ')
      MfLogger.log err
    end

    meeting_type = meeting_types.first
    

    content_type 'text/xml'

    RSS::Maker.make("atom") do |maker|
      maker.channel.author = "Manchester Citizenship"
      maker.channel.about = meeting_type.link
      maker.channel.title = " #{meeting_type.name} (Manchester City Council meetings)"
      maker.channel.description = "Data scraped from the Manchester City Council website in order to provide a list of meetings of the #{meeting_type.name}, and to allow notification of any new meetings being added."
      
      last_updated = add_meetings_to_feed maker, meeting_type, last_updated

      maker.channel.updated = get_feed_updated_time last_updated
    end.to_s
  end
  

  get '/' do
    result = '<h1>Manchester Citizenship</h1>'
    result = '<h2>List of available feeds</h2>'

    result += '<ul><li><a href="/feeds/mcc-meeting-types">Manchester City Council committees, groups and subgroups</a></li></ul>'

    result += '<ul><li><a href="/feeds/mcc-meeting/all">All meetings (Manchester City Council)</a></li></ul>'

    result += '<ul>'
    
    MeetingType.all.each do |mt|
      result += "<li><a href=\"/feeds/mcc-meeting/#{mt.url_friendly_name}\">#{mt.name} meetings (Manchester City Council)</a></li>"
    end
    
    result += '</ul>'
  end
end

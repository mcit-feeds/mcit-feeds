#!/usr/bin/env ruby
require 'nokogiri'
require 'open-uri'
require 'dm-core'
require 'dm-timestamps'
require 'dm-migrations'
require "rss"

# Add the path of the main app file to the load path
$LOAD_PATH.unshift( File.expand_path( File.dirname( __FILE__ ) ) )

require 'support/mf_logger.rb'
require 'support/datamapper_wrapper.rb'
require 'targets/mcc_page_locations.rb'

require 'models.rb'


class MccScraper
  include DatamapperWrapper
  
  def initialize
    # Get app root directory, in case app was started from another directory.
    app_directory = File.expand_path( File.dirname(__FILE__) )

    # Load config data.
    config = YAML::load_file( "#{app_directory}/.config.yml" )
    config[:root_directory] = app_directory

    # Modify any sqlite3 database name to explicitly be in app root directory.
    config[:db_name].prepend( "#{app_directory}/" ) if config[:db_type] == :sqlite3
    init_datamapper( config )
    
    DataMapper.auto_upgrade!

    Mail.defaults do
      delivery_method :sendmail
    end

    MfLogger.mail_from = config.delete :admin_email_from
    MfLogger.mail_to = config.delete :admin_email_to
    MfLogger.set_log_type( config.delete :log_type )
  end


  def go_scrape
    refresh_meeting_types
    refresh_meetings
    self
  end
  
  def latest_test
    MeetingType.all.each do | mt |
      puts mt.url_friendly_name
    end
  end

  private

  def refresh_meeting_types
    
    url = MccPageLocations.meeting_types

    begin
      doc = Nokogiri::HTML(open(url))
    rescue
      MfLogger.log "Exception opening #{url}"
      return
    end

    extracted_meeting_types = extract_meeting_types doc, url

    update_stored_meeting_types extracted_meeting_types if extracted_meeting_types
  end

  def extract_meeting_types doc, url

    content_section_css = 'section.page-meetings'
    content_section = doc.css( content_section_css )
    if content_section.nil? or content_section.empty?
      MfLogger.log "Failed to find content section '#{content_section_css}' on #{url}"
      return
    end

    meeting_types_list = content_section.css( 'ul' )[1]
    if meeting_types_list.nil?
      MfLogger.log "Failed to find meeting types list on #{url}"
      return
    elsif meeting_types_list.children.empty?
      MfLogger.log "Meeting types list seems to be empty on #{url}"
      return
    end

    meeting_types_list.css( 'a' ).each_with_object([]) do |link, all|
      mt = MeetingType.new
      mt.name = link.text.strip
      mt.link  = link.attribute('href')
      all << mt
    end
  end

  def update_stored_meeting_types extracted_meeting_types
    
    extracted_meeting_types.each do |extracted|
      
      existing = MeetingType.all( { name: extracted.name } )
      
      if existing.empty?
        unless extracted.save
          msg = 'Error while trying to save new meeting type.'
          extracted.errors.each { |e| e.each { |item| msg += "\n * #{item}" } }
          MfLogger.log msg
        end

      else
        mt = existing.first
        if mt.link != extracted.link
          mt.link = extracted.link
          unless mt.save
            msg = 'Error while trying to update meeting type link.'
            mt.errors.each { |e| e.each { |item| msg += "\n * #{item}" } }
            MfLogger.log msg
          end
        end
      end

    end
  end


  def refresh_meetings
    MeetingType.all.each do | mt |

      begin
        doc = Nokogiri::HTML(open(mt.link))
      rescue
        MfLogger.log "Exception opening the link for meetings of the #{mt.name} : #{mt.link} ."
        return
      end
      
      extracted_meetings = extract_meetings doc, mt

      update_stored_meetings( extracted_meetings, mt ) if extracted_meetings
    end
  end

  def extract_meetings doc, meeting_type

    err_suffix = "while looking for meetings of the #{meeting_type.name} at #{meeting_type.link}"

    content_section_css = '#content.container section article'
    content_section = doc.css( content_section_css )
    if content_section.nil? or content_section.empty?
      MfLogger.log "Failed to find content section '#{content_section_css} #{err_suffix}"
      return
    end

    meetings_list = content_section.css( 'ul' )[0]
    if meetings_list.nil?
      MfLogger.log "Failed to find meetings list #{err_suffix}"
      return
    elsif meetings_list.children.empty?
      MfLogger.log "Meetings list seems to be empty #{err_suffix}"
      return
    end

    meetings_list.css( 'a' ).each_with_object([]) do |link, all|
      m = Meeting.new
      m.date = Date.parse( link.text )
      m.link  = link.attribute('href')
      m.meeting_type = meeting_type
      all << m
    end
  end

  def update_stored_meetings extracted_meetings, meeting_type
    
    extracted_meetings.each do |extracted|
      
      existing = Meeting.all( { date: extracted.date, meeting_type: meeting_type } )
      
      if existing.empty?
        unless extracted.save
          msg = 'Error while trying to save new meeting.'
          extracted.errors.each { |e| e.each { |item| msg += "\n * #{item}" } }
          MfLogger.log msg
        end

      else
        m = existing.first
        if m.link != extracted.link
          m.link = extracted.link
          unless m.save
            msg = 'Error while trying to update meeting link.'
            m.errors.each { |e| e.each { |item| msg += "\n * #{item}" } }
            MfLogger.log msg
          end
        end
      end

    end
  end

end



MccScraper.new.go_scrape

#MccScraper.new.latest_test
